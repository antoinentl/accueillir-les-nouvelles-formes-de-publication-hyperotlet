# Accueillir les nouvelles formes de publication

## Constat
Certaines formes actuelles de publication dans le domaine académique tendent à se détacher des modèles classiques.
Depuis le format PDF jusqu'aux sites web en passant par le livre numérique au format EPUB, les plateformes de contenu, les applications ou les Progressive Web Applications, nous assistons à des formes moins figées – graphiquement et sémantiquement.

## Problème
Comment inscrire ces nouveaux objets numériques dans un environnement documentaire pensé d'abord pour des publications traditionnelles et majoritairement imprimées ?
Comment réunir ces artéfacts si dissemblables ?
Doit-on les rassembler _ensemble_ ?

## Esquisse de plan

1. Les nouvelles formes de publication : format EPUB, livres web, spécifcités des plateformes, Getty Publications et Distill.pub
2. Les difficultés intrinsèques : compatibilité de lecture (EPUB), versatilité (web), citabilité (mises à jour continues), etc.
3. L'opportunité du numérique : définition du numérique, force des métadonnées, puissance de l'archivage, possibilité du versionnement (identification des modifications)
4. Une question de design : recul nécessaire (dimension de dispositif et non de support), donner à voir (question des interfaces)

## Bibliographie

Blanc, J., & Haute, L. (2018). Technologies de l’édition numérique. Sciences du Design, n° 8(2), 11‑17.

Dacos, M. (Éd.). (2009). Read-write book: le livre inscriptible. Marseille, France: Centre pour l’édition électronique ouverte.

Epron, B., & Vitali-Rosati, M. (2018). L’édition à l’ère numérique. Paris: La Découverte. Consulté à l’adresse https://www.cairn.info/l-edition-a-l-ere-numerique--9782707199355.htm

Fauchié, A. (2015, juillet 7). From—To : une publication contemporaine. Consulté 30 juin 2018, à l’adresse https://www.quaternum.net/2015/07/07/from-to/

Fauchié, A. (2017). Le livre web comme objet d’édition ? In Design et innovation dans la chaîne du livre. PUF.

Guichard, É. (2008). L’écriture scientifique. Présenté à L’Internet : Espace public et Enjeux de connaissance. Consulté à l’adresse https://halshs.archives-ouvertes.fr/halshs-00347616/document

Haute, L., & Blanc, J. (2018). Publier la recherche en design : (hors-)normes, (contre-)formats, (anti-)standards. Réel-Virtuel, (6). Consulté à l’adresse http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design

Masure, A. (2018, avril). Mutations du livre Web. Montréal, Canada. Consulté à l’adresse http://www.anthonymasure.com/conferences/2018-04-livre-web-ecridil-montreal

Mourat, R. de. (2018). Le design fantomatique des communautés savantes : enjeux phénoménologiques, sociaux et politiques de trois formats de données en usage dans l’édition scientifique contemporaine. Sciences du Design, n° 8(2), 34‑44.

Simondon, G. (2012). Du mode d’existence des objets techniques. Paris, France: Aubier, impr. 2012.

Stern, N., Guédon, J.-C., & Jensen, T. W. (2015). Crystals of Knowledge Production. An Intercontinental Conversation about Open Science and the Humanities. Nordic Perspectives on Open Science, 1(0), 1‑24. https://doi.org/10.7557/11.3619

Vitali-Rosati, M. (2018, mai 2). Pour une définition de l’éditorialisation. Consulté 13 février 2019, à l’adresse http://etudes-digitales.fr/pour-une-definition-de-leditorialisation/


## Questions en suspens

- Les parties 1 et 2 doivent-elles être réunies ? L'objectif est de bien distinguer ce que sont les nouvelles formes, puis les questions qu'elles posent.
- Le sujet n'a-t-il pas été traité ailleurs ?...
- Est-ce que la dimension de forme n'est pas trop importante ? Notamment par rapport au domaine de la documentation et au principe d'intention des auteurs/éditeurs ?
